#!/bin/sh
# Read the current version from package.json
current_version=$(jq -r '.version' package.json)

# Split the version into major, minor, and patch components
IFS='.' read -r -a version_parts <<< "$current_version"
major=${version_parts[0]}
minor=${version_parts[1]}
patch=${version_parts[2]}

# Increment the patch version
patch=$((patch + 1))

# Create the new version string
new_version="$major.$minor.$patch"

# Update the version in package.json
jq --arg new_version "$new_version" '.version = $new_version' package.json > temp.json && mv temp.json package.json

# Commit the new version to the repository
git config user.email "${GITLAB_USER_EMAIL}"
git config user.name "${GITLAB_USER_NAME}"
git add package.json
git commit -m "Increment version to $new_version"

# Create a new Git tag
git tag "v$new_version"
git push origin main --tags
